import { GetStaticPaths, GetStaticProps } from "next";
import { useRouter } from "next/router";
import { createNamedExports } from "typescript";

export default function Member({ user }) {
	const { query, isFallback } = useRouter();

	if (isFallback) {
		return <p>Loading...</p>;
	}

	return (
		<div>
			<h1>{query.login}</h1>
			<h3>{user.name}</h3>
			<p>{user.bio}</p>
		</div>
	);
}

export const getStaticPaths: GetStaticPaths = async () => {
	let paths = [];

	try {
		const response = await fetch(
			`https://api.github.com/orgs/rocketseat/members`
		);

		const data = await response.json();

		paths = data.map((member) => {
			return { params: { login: member.login } };
		});
	} catch (e) {}

	return {
		paths,
		fallback: true,
	};
};

export const getStaticProps: GetStaticProps = async (context) => {
	const { login } = context.params;

	const response = await fetch(`https://api.github.com/users/${login}`);
	const data = await response.json();

	return {
		props: {
			user: data,
		},
		revalidate: 60,
	};
};
